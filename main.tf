terraform {
    required_version = ">= 0.12"
}

provider "aws" {
    region = var.aws_region
}

# Cria VPC
resource "aws_vpc" "default" {
    cidr_block = "10.0.0.0/16"
}

# Cria Intenet Gateway
resource "aws_intenet_gateway" "default" {
    vpc_id = aws_vpc.default.id
}

# Cria rotas
resource "aws_route" "internet_access" {
    route_table_id = aws_vpc.default.main_route_table_id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default.id
}

# Cria subrede
resource "aws_subnet" "default" {
    vpc_id = aws_vpc.default.id
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = true
}

# Cria Security Group ELB 
resource "aws_security_group" "elb" {
    name = "terraform_example_elb"
    description = "Used in the terraform"
    vpc_id = aws_vpc.default.id

# HTTP access from anywhere
ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}

# outbound internet access
egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
}
}

# Cria Security Group

resource "aws_security_group" "default" {
    name = "terraform_example"
    description = "Used in the terrafeorm"
    vpc_id = "aws_vpc.default".id

    # SSH access from anywhere
ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}

# HTTP access from the VPC
egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
}

# outbound internet access
egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
}
}

# Cria ELB _ Elastic Load Balancer
resource "aws_elb" "web" {
    name = "terraform-example-elb"

    subnets = [aws_subnet.default.id]
    security_groups = [aws_security_group.elb.id]
    instances = [aws_instance.web.id]

    listener {
        instance_port = 80
        instance_protocol = "http"
        lb_port = 80
        lb_protocol = "http"
    }
}

# Criar Key Pair
resource "aws_key_pair" "auth" {
    key_name = var.key_name
    public_key = file(var.public_key_path)
}

# Cria Instance EC2 (VM)
resource "aws_instance" "web" {
    # The connection block tells our provisioner how to connect
    # communicate with the resource (instance)
    connection {
      type = "ssh"
      # the default username for our AMI
      user = "ubuntu"
      host = self.public_id
      # The connection will use the local SSh agent for authentication.
    }

    instance_type = "t2.micro"
    # Lookup the correct AMI based on the region
    # we specified
    ami = var.aws_amis [var.aws_region]

    #The name of our SSh keypair we created above.
    key_name = aws_key_pair.auth.id

    # Our Security group to allow HTTP and SSH access.
    vpc_security_group_ids = [aws_security_group.default.id]

    subnet_id = aws_subnet.default.id
}